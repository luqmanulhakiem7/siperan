<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vehicleRequest extends Model
{
    use HasFactory;
    protected $fillable = ['vehicle_id','driver_id','manager1_id','manager2_id','approve1_id', 'approve2_id', 'tgl_disetujui'];
}
