<?php

namespace App\Http\Controllers;

use App\Models\vehicleRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $year = Carbon::now()->format('Y');
        $jan = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '01')->count();
        $feb = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '02')->count();
        $mar = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '03')->count();
        $apr = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '04')->count();
        $mei = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '05')->count();
        $jun = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '06')->count();
        $jul = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '07')->count();
        $agu = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '08')->count();
        $sep = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '09')->count();
        $okt = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '10')->count();
        $nov = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '11')->count();
        $des = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '12')->count();
        $jan2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '01')->count();
        $feb2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '02')->count();
        $mar2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '03')->count();
        $apr2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '04')->count();
        $mei2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '05')->count();
        $jun2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '06')->count();
        $jul2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '07')->count();
        $agu2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '08')->count();
        $sep2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '09')->count();
        $okt2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '10')->count();
        $nov2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '11')->count();
        $des2 = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->whereYear('tgl_disetujui', $year)->whereMonth('tgl_disetujui', '12')->count();
        $arrMonth = [$jan, $feb, $mar, $apr, $mei, $jun, $jul, $agu, $sep, $okt, $nov, $des];
        $arrMonth2 = [$jan2, $feb2, $mar2, $apr2, $mei2, $jun2, $jul2, $agu2, $sep2, $okt2, $nov2, $des2];
        $pesanApprove = implode(',', $arrMonth);
        $pesanTolak = implode(',', $arrMonth2);


        $totalAprove = 0;
        $totalTolak = 0;
        for ($i=0; $i < count($arrMonth) ; $i++) { 
            $totalAprove += $arrMonth[$i];
        };
        for ($j=0; $j < count($arrMonth2) ; $j++) { 
            $totalTolak += $arrMonth2[$j];
        };
        $totalPemesanan = $totalAprove + $totalTolak;
        
        $totalPesan = vehicleRequest::count();

        $totalWaiting = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '0')
            ->where('approve2_id', '0');
        })->count();
        $totalAcc = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '1')
            ->where('approve2_id', '1');
        })->count();
        $totalNotAcc = vehicleRequest::where(function ($q) {
            $q->where('approve1_id', '2')
            ->orWhere('approve2_id', '2');
        })->count();
        return view('pages.Dashboard.index', compact(['pesanApprove', 'pesanTolak', 'totalPemesanan','totalNotAcc', 'totalAcc','totalWaiting', 'totalPesan']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
