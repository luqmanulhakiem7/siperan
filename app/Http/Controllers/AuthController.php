<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function register()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $validated = $request->validate([
            'email' => 'required',
            'password' => 'required|min: 8'
        ]);

        if (!$validated) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Email dan Password Harus Diisi!!!']);
        }else{
            if ($email) {
                $check = User::where('email', $email)->first();
                if (!empty($check)) {
                    if (Hash::check($password, $check->password)) {
                        Auth::attempt(['email' => $email, 'password' => $password]);
                        return redirect('/dashboard');
                    }else{
                        return redirect()->back()->withInput()->withErrors(['message' => 'password anda salah']);
                    }
                } else {
                    return redirect()->back()->withInput()->withErrors(['message' => 'email anda tidak terdaftar']);
                }
            }
        }        
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
