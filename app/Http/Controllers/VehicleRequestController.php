<?php

namespace App\Http\Controllers;

use App\Exports\LaporanExport;
use App\Models\driver;
use App\Models\Profile;
use App\Models\vehicle;
use App\Models\vehicleRequest;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class VehicleRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $driver = driver::all();
        $kendaraan = vehicle::all();
        $manager = Profile::all();
        return view('pages.Approval.index', compact(['driver', 'kendaraan', 'manager']));
    }

    public function approve()
    {
        $id = auth()->user()->id;
        $data = DB::table('vehicle_requests as vr')
        ->join('vehicles as v', 'v.id', '=', 'vr.vehicle_id')
        ->join('drivers as d', 'd.id', '=', 'vr.driver_id')
        ->join('profiles as p1', 'p1.id', '=', 'vr.manager1_id')
        ->join('profiles as p2', 'p2.id', '=', 'vr.manager2_id')
        ->where(function ($query) use ($id) {
            $query->where('vr.manager1_id', '=', $id)
            ->orWhere('vr.manager2_id', '=', $id);
        })
        ->where(function ($q) {
            $q->where('vr.approve1_id', '=', '0')
            ->orWhere('vr.approve2_id', '=', '0');
        })
        ->select('vr.*', 'd.first_name as fNameD', 'd.last_name as lNameD', 'v.branch', 'v.plat', 'p1.first_name as fNameM1', 'p1.last_name as lNameM1', 'p2.first_name as fNameM2', 'p2.last_name as lNameM2')
        ->paginate(50);

        return view('pages.Approval.approve', compact('data'));
    }
    public function waiting ()
    {
        $id = auth()->user()->id;
        $data = DB::table('vehicle_requests as vr')
        ->join('vehicles as v', 'v.id', '=', 'vr.vehicle_id')
        ->join('drivers as d', 'd.id', '=', 'vr.driver_id')
        ->join('profiles as p1', 'p1.id', '=', 'vr.manager1_id')
        ->join('profiles as p2', 'p2.id', '=', 'vr.manager2_id')
        ->where(function ($q) {
            $q->where('vr.approve1_id', '=', '0')
            ->orWhere('vr.approve2_id', '=', '0');
        })
        ->select('vr.*', 'd.first_name as fNameD', 'd.last_name as lNameD', 'v.branch', 'v.plat', 'p1.first_name as fNameM1', 'p1.last_name as lNameM1', 'p2.first_name as fNameM2', 'p2.last_name as lNameM2')
        ->paginate(50);

        return view('pages.Approval.waiting', compact('data'));

    }
    public function history ()
    {
        $id = auth()->user()->id;
        $data = DB::table('vehicle_requests as vr')
        ->join('vehicles as v', 'v.id', '=', 'vr.vehicle_id')
        ->join('drivers as d', 'd.id', '=', 'vr.driver_id')
        ->join('profiles as p1', 'p1.id', '=', 'vr.manager1_id')
        ->join('profiles as p2', 'p2.id', '=', 'vr.manager2_id')
        ->where(function ($query) use ($id) {
            $query->where('vr.manager1_id', '=', $id)
            ->orWhere('vr.manager2_id', '=', $id);
        })
        ->select('vr.*', 'd.first_name as fNameD', 'd.last_name as lNameD', 'v.branch', 'v.plat', 'p1.first_name as fNameM1', 'p1.last_name as lNameM1', 'p2.first_name as fNameM2', 'p2.last_name as lNameM2')
        ->paginate(50);

        return view('pages.Approval.managerhistory', compact('data'));
    }

    public function indexApprove ()
    {
        $data = DB::table('vehicle_requests as vr')
        ->join('vehicles as v', 'v.id', '=', 'vr.vehicle_id')
        ->join('drivers as d', 'd.id', '=', 'vr.driver_id')
        ->join('profiles as p1', 'p1.id', '=', 'vr.manager1_id')
        ->join('profiles as p2', 'p2.id', '=', 'vr.manager2_id')
        ->where(function ($q) {
            $q->where('vr.approve1_id', '=', '1')
            ->where('vr.approve2_id', '=', '1');
        })
        ->select('vr.*', 'd.first_name as fNameD', 'd.last_name as lNameD', 'v.branch', 'v.plat', 'p1.first_name as fNameM1', 'p1.last_name as lNameM1', 'p2.first_name as fNameM2', 'p2.last_name as lNameM2')
        ->paginate(50);

        return view('pages.Approval.indexapprove', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $d = $request->driver;
        $k = $request->kendaraan;
        $m1 = $request->manager1;
        $m2 = $request->manager2;
        $validated = $request->validate([
            'driver' => 'required',
            'kendaraan' => 'required',
            'manager1' => 'required',
            'manager2' => 'required',
        ]);

        if (!$validated) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Semua Field Harus Diisi!!!']);
        }

        $data = vehicleRequest::create([
            'driver_id' => $d,
            'vehicle_id' => $k,
            'manager1_id' => $m1,
            'manager2_id' => $m2,
            'tgl_disetujui' => Carbon::now()
        ]);

        return redirect('pemesanan-kendaraan');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, vehicleRequest $vehicleRequest, $idRequest, $idApprove)
    {
        $find = vehicleRequest::find($idRequest);
        if ($find) {
            $user = auth()->user()->id;
            if ($find->manager1_id == $user) {
                $dt = [
                    'approve1_id' => $idApprove
                ];
                $find->update($dt);

                return redirect('/approve');
            } else {
                $dt = [
                    'approve2_id' => $idApprove
                ];
                $find->update($dt);
                return redirect('/approve');

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function export()
    {
        return Excel::download(new LaporanExport, 'laporan.xlsx');

        return redirect()->back();
    }
}
