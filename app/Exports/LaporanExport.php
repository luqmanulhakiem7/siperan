<?php

namespace App\Exports;

use App\Models\vehicleRequest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LaporanExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return vehicleRequest::where(function ($q){
        //     $q->where('approve1_id', '1')
        //     ->where('approve2_id', '1');
        // })->get();
        $data = DB::table('vehicle_requests as vr')
        ->join('vehicles as v', 'v.id', '=', 'vr.vehicle_id')
        ->join('drivers as d', 'd.id', '=', 'vr.driver_id')
        ->leftJoin('profiles as p1', 'p1.id', '=', 'vr.manager1_id')
        ->rightJoin('profiles as p2', 'p2.id', '=', 'vr.manager2_id')
        ->where(function ($q) {
            $q->where('vr.approve1_id', '=', '1')
            ->where('vr.approve2_id', '=', '1');
        })
        ->selectRaw("vr.tgl_disetujui, CONCAT(d.first_name, ' ', d.last_name) as nama_supir, CONCAT(v.branch, ' [ ', v.plat, ' ]') as angkutan, CONCAT(p1.first_name, ' ', p1.last_name) as manager1, CONCAT(p2.first_name, ' ', p2.last_name) as manager2")
        ->get();
        // dd($data);
        return $data;
    }
    public function headings(): array
    {
        return [
            'tgl_disetujui',
            'angkutan',
            'supir',
            'pihak yang menyetujui 1',
            'pihak yang menyetujui 2',
        ];
    }
}
