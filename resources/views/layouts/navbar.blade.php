<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="/dashboard">
              <i class="mdi mdi-grid-large menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          {{-- <li class="nav-item nav-category">Admin</li> --}}
          @if (auth()->user()->level == '1')
              
            <li class="nav-item">
              <a class="nav-link" href="/pemesanan-kendaraan">
                <i class="menu-icon mdi mdi-file-document-edit"></i>
                <span class="menu-title">Pemesanan Kendaraan</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/waiting-approval">
                <i class="menu-icon mdi mdi-history"></i>
                <span class="menu-title">Menunggu Approve</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/laporan-approval">
                <i class="menu-icon mdi mdi-book-multiple"></i>
                <span class="menu-title">Laporan</span>
              </a>
            </li>
          @endif

          @if (auth()->user()->level == '2')
          {{-- <li class="nav-item nav-category">Manager</li> --}}
          <li class="nav-item">
            <a class="nav-link" href="/approve">
              <i class="menu-icon mdi mdi-file-document"></i>
              <span class="menu-title">Permintaan Persetujuan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/history-approval">
              <i class="menu-icon mdi mdi-file-document"></i>
              <span class="menu-title">History</span>
            </a>
          </li>
          @endif

           {{-- <li class="nav-item nav-category">Master Data</li>
          <li class="nav-item">
            <a class="nav-link" href="/driver">
              <i class="menu-icon mdi mdi-account-multiple"></i>
              <span class="menu-title">Driver</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="assets/#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-truck"></i>
              <span class="menu-title"> Angkutan</span>
              <i class="menu-arrow"></i> 
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/angkutan">Semua Angkutan</a></li>
                <li class="nav-item"> <a class="nav-link" href="/angkutan-barang">Angkutan Barang</a></li>
                <li class="nav-item"> <a class="nav-link" href="/angkutan-orang">Angkutan Orang</a></li>
              </ul>
            </div>
          </li> --}}
        </ul>
      </nav>