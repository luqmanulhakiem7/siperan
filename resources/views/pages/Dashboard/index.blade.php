@extends('index')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-sm-12">
              <div class="home-tab">
                <div class="tab-content tab-content-basic">
                  <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview"> 
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="statistics-details d-flex align-items-center justify-content-between">
                          
                          <div>
                            <p class="statistics-title">Permintaan Pemesanan Kendaraan</p>
                            <h3 class="rate-percentage">{{$totalPesan}}</h3>
                          </div>
                          <div>
                            <p class="statistics-title">Pemesanan Menunggu Persetujuan</p>
                            <h3 class="rate-percentage">{{$totalWaiting}}</h3>
                          </div>
                          <div>
                            <p class="statistics-title">Pemesanan Yang Disetujui</p>
                            <h3 class="rate-percentage">{{$totalAcc}}</h3>
                          </div>
                          <div>
                            <p class="statistics-title">Pemesanan Yang Ditolak</p>
                            <h3 class="rate-percentage">{{$totalNotAcc}}</h3>
                          </div>
                        </div>
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col-lg-12 d-flex flex-column">
                        <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                  <div>
                                    <h4 class="card-title card-title-dash">Grafik Pemesanan Kendaraan</h4>
                                  </div>
                                  <div>
                                    <span class="btn btn-lg btn-secondary">Tahun Ini</span>
                                  </div>
                                </div>
                                <div class="d-sm-flex align-items-center mt-1 justify-content-between">
                                  <div class="d-sm-flex align-items-center mt-4 justify-content-between"><h2 class="me-2 fw-bold">{{$totalPemesanan}}</h2><h4 class="me-2">Pemesanan</h4></div>
                                  <div class="me-3"><div id="marketing-overview-legend"></div></div>
                                </div>
                                <div class="chartjs-bar-wrapper mt-3">
                                  <canvas id="pemesananOverview"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
        <!-- partial -->
      </div>
@endsection

@push('scriptku')
      <script>
          if ($("#pemesananOverview").length) {
            var pesanApprove = [ {{$pesanApprove}} ];
            var pesanTolak = [ {{$pesanTolak}} ];
            var pemesananOverviewChart = document
                .getElementById("pemesananOverview")
                .getContext("2d");
            var pemesananOverviewData = {
                labels: [
                    "JAN",
                    "FEB",
                    "MAR",
                    "APR",
                    "MAY",
                    "JUN",
                    "JUL",
                    "AUG",
                    "SEP",
                    "OCT",
                    "NOV",
                    "DEC",
                ],
                datasets: [
                    {
                        label: "Ditolak",
                        data: pesanTolak,
                        backgroundColor: "#52CDFF",
                        borderColor: ["#52CDFF"],
                        borderWidth: 0,
                        fill: true, // 3: no fill
                    },
                    {
                        label: "Disetujui",
                        data: pesanApprove,
                        backgroundColor: "#1F3BB3",
                        borderColor: ["#1F3BB3"],
                        borderWidth: 0,
                        fill: true, // 3: no fill
                    },
                ],
            };

            var pemesananOverviewOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                        {
                            gridLines: {
                                display: true,
                                drawBorder: false,
                                color: "#F0F0F0",
                                zeroLineColor: "#F0F0F0",
                            },
                            ticks: {
                                beginAtZero: true,
                                autoSkip: true,
                                maxTicksLimit: 5,
                                fontSize: 10,
                                color: "#6B778C",
                            },
                        },
                    ],
                    xAxes: [
                        {
                            stacked: true,
                            barPercentage: 0.35,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 12,
                                fontSize: 10,
                                color: "#6B778C",
                            },
                        },
                    ],
                },
                legend: false,
                legendCallback: function (chart) {
                    var text = [];
                    text.push('<div class="chartjs-legend"><ul>');
                    for (var i = 0; i < chart.data.datasets.length; i++) {
                        console.log(chart.data.datasets[i]); // see what's inside the obj.
                        text.push('<li class="text-muted text-small">');
                        text.push(
                            '<span style="background-color:' +
                                chart.data.datasets[i].borderColor +
                                '">' +
                                "</span>"
                        );
                        text.push(chart.data.datasets[i].label);
                        text.push("</li>");
                    }
                    text.push("</ul></div>");
                    return text.join("");
                },

                elements: {
                    line: {
                        tension: 0.4,
                    },
                },
                tooltips: {
                    backgroundColor: "rgba(31, 59, 179, 1)",
                },
            };
            var pemesananOverview = new Chart(pemesananOverviewChart, {
                type: "bar",
                data: pemesananOverviewData,
                options: pemesananOverviewOptions,
            });
            document.getElementById("marketing-overview-legend").innerHTML =
                pemesananOverview.generateLegend();
        }

        </script>
@endpush