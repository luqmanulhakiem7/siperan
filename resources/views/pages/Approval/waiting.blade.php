@extends('index')

@section('content')
@if (auth()->user()->level == '1')
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="row">
        {{-- <div class="col-lg-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Basic Table</h4>
              <p class="card-description">
                Add class <code>.table</code>
              </p>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Profile</th>
                      <th>VatNo.</th>
                      <th>Created</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Jacob</td>
                      <td>53275531</td>
                      <td>12 May 2017</td>
                      <td><label class="badge badge-danger">Pending</label></td>
                    </tr>
                    <tr>
                      <td>Messsy</td>
                      <td>53275532</td>
                      <td>15 May 2017</td>
                      <td><label class="badge badge-warning">In progress</label></td>
                    </tr>
                    <tr>
                      <td>John</td>
                      <td>53275533</td>
                      <td>14 May 2017</td>
                      <td><label class="badge badge-info">Fixed</label></td>
                    </tr>
                    <tr>
                      <td>Peter</td>
                      <td>53275534</td>
                      <td>16 May 2017</td>
                      <td><label class="badge badge-success">Completed</label></td>
                    </tr>
                    <tr>
                      <td>Dave</td>
                      <td>53275535</td>
                      <td>20 May 2017</td>
                      <td><label class="badge badge-warning">In progress</label></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Hoverable Table</h4>
              <p class="card-description">
                Add class <code>.table-hover</code>
              </p>
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>User</th>
                      <th>Product</th>
                      <th>Sale</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Jacob</td>
                      <td>Photoshop</td>
                      <td class="text-danger"> 28.76% <i class="ti-arrow-down"></i></td>
                      <td><label class="badge badge-danger">Pending</label></td>
                    </tr>
                    <tr>
                      <td>Messsy</td>
                      <td>Flash</td>
                      <td class="text-danger"> 21.06% <i class="ti-arrow-down"></i></td>
                      <td><label class="badge badge-warning">In progress</label></td>
                    </tr>
                    <tr>
                      <td>John</td>
                      <td>Premier</td>
                      <td class="text-danger"> 35.00% <i class="ti-arrow-down"></i></td>
                      <td><label class="badge badge-info">Fixed</label></td>
                    </tr>
                    <tr>
                      <td>Peter</td>
                      <td>After effects</td>
                      <td class="text-success"> 82.00% <i class="ti-arrow-up"></i></td>
                      <td><label class="badge badge-success">Completed</label></td>
                    </tr>
                    <tr>
                      <td>Dave</td>
                      <td>53275535</td>
                      <td class="text-success"> 98.05% <i class="ti-arrow-up"></i></td>
                      <td><label class="badge badge-warning">In progress</label></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div> --}}
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
            <div class="row mb-3">
                <div class="col-lg-6">
                    <h4 class="card-title">Menunggu Persetujuan</h4>
                </div>
                {{-- <div class="col-lg-8"></div> --}}
                {{-- <div class="col-lg-2">
                    <a href="" class="btn btn-sm btn-primary">Tambah Driver</a>
                </div> --}}
            </div>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                        <th>#</th>
                      <th>
                        Kendaraan
                      </th>
                      <th>
                        Driver
                      </th>
                      <th>
                        Manager 1
                      </th>
                      <th>
                        Manager 2
                      </th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $count = count($data);
                    $no = 1;
                    ?>
                    @if ($count > 0)
                        @foreach ($data as $item)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$item->branch}} [ {{$item->plat}} ]</td>
                                <td>{{$item->fNameD}} {{$item->lNameD}}</td>
                                                                    <td>
                                  @if ($item->approve1_id === '0')
                                      <span class="badge bg-warning">
                                          {{$item->fNameM1}} {{$item->lNameM1}}
                                      </span>
                                    @endif
                                    @if ($item->approve1_id === '1')
                                        <span class="badge bg-primary">
                                            {{$item->fNameM1}} {{$item->lNameM1}}
                                        </span>
                                    @endif
                                  @if ($item->approve1_id === '2')
                                        <span class="badge bg-danger">
                                            {{$item->fNameM1}} {{$item->lNameM1}}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    @if ($item->approve2_id === '0')
                                      <span class="badge bg-warning">
                                          {{$item->fNameM2}} {{$item->lNameM2}}
                                      </span>
                                    @endif
                                    @if ($item->approve2_id === '1')
                                        <span class="badge bg-primary">
                                            {{$item->fNameM2}} {{$item->lNameM2}}
                                        </span>
                                    @endif
                                    @if ($item->approve2_id === '2')
                                        <span class="badge bg-danger">
                                            {{$item->fNameM2}} {{$item->lNameM2}}
                                        </span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <span class="badge bg-warning">Pending</span>
                                </td>
                            </tr>   
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">Data tidak ditemukan</td>
                        </tr>
                    @endif
                  </tbody>
            </table>
            <div class="mt-5">
                {{$data->links('vendor.pagination.bootstrap-5')}}
            </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:../../partials/_footer.html -->
    <!-- partial -->
  </div>
@else 
<script>
    window.location = '404';
  </script>
@endif
@endsection