@extends('index')

@section('content')
  @if (auth()->user()->level == '1')
     <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Form Pemesanan Kendaraan</h4>
                  {{-- <p class="card-description">
                    Basic form layout
                  </p> --}}
                  <form action="{{route('simpan-pemesanan')}}" method="POST" class="forms-sample">
                    @csrf
                    <div class="form-group">
                      <label for="exampleInputEmail1">Driver</label>
                      <select name="driver" id="driver" class="form-control">
                        <option value="">-- Pilih Driver --</option>
                        @foreach ($driver as $item)
                          <option value="{{$item->id}}">{{$item->first_name}} {{$item->last_name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Angkutan</label>
                      <select name="kendaraan" id="angkutan" class="form-control">
                        <option value="">-- Pilih Angkutan --</option>
                         @foreach ($kendaraan as $item)
                          <option value="{{$item->id}}">{{$item->branch}} [ {{$item->plat}} ]</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Manager 1</label>
                      <select name="manager1" id="manager1" class="form-control">
                        <option value="">-- Pilih Manager 1 --</option>
                         @foreach ($manager as $item)
                          <option value="{{$item->id}}">{{$item->first_name}} {{$item->last_name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Manager 2</label>
                      <select name="manager2" id="manager2" class="form-control">
                        <option value="">-- Pilih Manager 2 --</option>
                        @foreach ($manager as $item)
                          <option value="{{$item->id}}">{{$item->first_name}} {{$item->last_name}}</option>
                        @endforeach
                      </select>
                    </div>
                    
                    <button type="submit" class="btn btn-primary me-2" id="myButton">Submit</button>
                    {{-- <button class="btn btn-light">Cancel</button> --}}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
      </div>  
  @else
  <script>
    window.location = '404';
  </script>
  @endif
@endsection

@push('scriptme')
    <script>
      (function (){
        const myButton = document.getElementById('myButton');
        var field = document.querySelectorAll('#driver, #angkutan, #manager1, #manager2');
        myButton.disabled = true;
        console.log($('#angkutan').val() != "");
        let check = () => {
          if ($('#angkutan').val() != "" && $('#driver').val() != "" &&  $('#manager1').val() != "" && $('#manager2  ').val() != ""){
            myButton.disabled = false;
          } else {
            myButton.disabled = true;
          }
        }
        for (let i = 0; i < field.length; i++) {
          field[i].addEventListener('change', check);
        }
      })();
    </script>
@endpush