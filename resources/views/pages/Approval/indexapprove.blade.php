@extends('index')

@section('content')
@if (auth()->user()->level == '1')
<div class="main-panel">
   <div class="content-wrapper">
     <div class="row">
       <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
           <div class="card-body">
           <div class="row mb-3">
               <div class="col-lg-6">
                   <h4 class="card-title">Laporan Pemesanan Kendaraan</h4>
               </div>
               <div class="col-lg-4"></div>
               <div class="col-lg-2">
                <a href="laporan/export" class="btn btn-sm btn-primary">Download Laporan</a>
               </div>
           </div>
             <div class="table-responsive">
               <table class="table table-striped">
                 <thead>
                   <tr>
                       <th>#</th>
                       <th>
                           Tanggal Disetujui
                       </th>
                     <th>
                       Kendaraan
                     </th>
                     <th>
                       Driver
                     </th>
                     <th>
                       Manager 1
                     </th>
                     <th>
                       Manager 2
                     </th>
                   </tr>
                 </thead>
                 <tbody>
                   <?php 
                   $count = count($data);
                   $no = 1;
                   ?>
                   @if ($count > 0)
                       @foreach ($data as $item)
                           <tr>
                               <td>{{$no++}}</td>
                               <td>{{$item->tgl_disetujui}}</td>
                               <td>{{$item->branch}} [ {{$item->plat}} ]</td>
                               <td>{{$item->fNameD}} {{$item->lNameD}}</td>
                               <td>
                                   {{$item->fNameM1}} {{$item->lNameM1}}
                               </td>
                               <td>
                                   {{$item->fNameM2}} {{$item->lNameM2}}
                               </td>
                           </tr>   
                       @endforeach
                   @else
                       <tr>
                           <td colspan="6" class="text-center">Data tidak ditemukan</td>
                       </tr>
                   @endif
                 </tbody>
           </table>
           <div class="mt-5">
               {{$data->links('vendor.pagination.bootstrap-5')}}
           </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <!-- content-wrapper ends -->
   <!-- partial:../../partials/_footer.html -->
   <!-- partial -->
 </div>
    
@else
    <script>
    window.location = '404';
  </script>
@endif
@endsection