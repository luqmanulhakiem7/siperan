<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Tentang SIPERAN

Siperan adalah aplikasi pemesanan kendaraan berbasis website. aplikasi ini bertujuan untuk mengajukan persetujuan dan mencatat persetujuan untuk penggunaan angkutan pada perusahaan nikel.

Teknologi yang digunakan dalam pembuatan aplikasi SIPERAN

-   [MariaDB v10.6 dan MySQL](https://mariadb.com/downloads/)
-   Php v.8.1.12
-   [Framework Laravel v.10](https://laravel.com/docs/10.x)
    Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Username dan Password

**Role Admin**

-   Username : admin@app.com || Password : password

**Role Pihak Yang Menyetujui (Manager)**

-   Username : managera@app.com || Password : password
-   Username : managerb@app.com || Password : password

## Panduan Penggunaan SIPERAN

-   1. Clone Repository SIPERAN pada branch master.
-   2. jalankan perintah **composer install** untuk menginstall projectnya.
-   3. ubah nama file **.env.example** menjadi **.env**.
-   4. lakukan konfigurasi database file **.env**.
-   5. jalankan perintah **php artisan migrate:fresh --seed** untuk migrate database dan membuat data dummy.
-   6. jalankan perintah **php artisan serve** untuk menjalankan project.
-   7. Login menggunakan akun dengan role admin
-   8. buat pemesanan kendaraan pada menu **Pemesanan Kendaraan** yang berada pada dashboard admin.
-   9. admin dapat melihat status persetujuan pemesanan pada menu **Menunggu Approve**.
-   10. logout akun dengan role admin.
-   11. login menggunakan akun dengan role manager.
-   12. lakukan persetujuan pada menu **Permintaan Persetujuan** pada dashboard manager.
-   13. manager dapat melihat history interaksi permintaan persetujuan yang sudah dilakukan pada menu **history**
-   14. lakukan 2x persetujuan pada akun manager
-   15. apabila disetujui oleh kedua manager maka permintaan akan tampil pada menu **Laporan** yang terletak di dashboard admin
-   16. admin dapat mendownload semua laporan pada menu **Laporan** dan menghasilkan output excel
