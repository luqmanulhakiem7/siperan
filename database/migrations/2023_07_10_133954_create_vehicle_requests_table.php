<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vehicle_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vehicle_id')->unsigned();
            $table->bigInteger('driver_id')->unsigned();
            $table->bigInteger('manager1_id')->unsigned();
            $table->bigInteger('manager2_id')->unsigned();
            $table->enum('approve1_id', ['0','1','2'])->default('0');
            $table->enum('approve2_id', ['0','1','2'])->default('0');
            $table->date('tgl_disetujui');
            $table->timestamps();

            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onUpdate('cascade');
            $table->foreign('driver_id')->references('id')->on('drivers')->onUpdate('cascade');
            $table->foreign('manager1_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('manager2_id')->references('id')->on('users')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vehicle_requests');
    }
};
