<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('branch')->nullable();
            $table->string('plat')->unique();
            $table->enum('hakmilik', ['hak milik','sewa']);
            $table->enum('jenis', ['angkutan barang','angkutan orang']);
            $table->enum('status', ['tersedia','digunakan'])->default('tersedia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vehicles');
    }
};
