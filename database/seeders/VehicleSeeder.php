<?php

namespace Database\Seeders;

use App\Models\vehicle;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $arr = [
            ['branch' => 'Mercedes', 'plat' => '2112', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
            ['branch' => 'Mercedes', 'plat' => '1111', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
            ['branch' => 'Mercedes', 'plat' => '2131', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
            ['branch' => 'Mercedes', 'plat' => '1123', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
            ['branch' => 'Honda', 'plat' => '2321', 'hakmilik' => 'sewa', 'jenis' => 'angkutan orang'],
            ['branch' => 'Honda', 'plat' => '1421', 'hakmilik' => 'sewa', 'jenis' => 'angkutan orang'],
            ['branch' => 'Honda', 'plat' => '1321', 'hakmilik' => 'sewa', 'jenis' => 'angkutan orang'],
            ['branch' => 'Mercedes', 'plat' => '1324', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
            ['branch' => 'Mercedes', 'plat' => '1124', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
            ['branch' => 'Mercedes', 'plat' => '1231', 'hakmilik' => 'hak milik', 'jenis' => 'angkutan barang'],
        ];

        $vehicle = vehicle::insert($arr);
    }
}
