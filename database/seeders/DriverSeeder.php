<?php

namespace Database\Seeders;

use App\Models\driver;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DriverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $arrayku = [
            ['first_name' => 'Luqmanul', 'last_name' => 'Hakiem', 'nip' => '12345678'],
            ['first_name' => 'Dinosaurus','last_name' => '', 'nip' => '22345678'],
            ['first_name' => 'T-Rex','last_name' => '', 'nip' => '32345678'],
            ['first_name' => 'Kucing','last_name' => '', 'nip' => '42345678'],
            ['first_name' => 'Tupai','last_name' => '', 'nip' => '52345678'],
            ['first_name' => 'Ular','last_name' => '', 'nip' => '62345678'],
            ['first_name' => 'Miaw','last_name' => '', 'nip' => '72345678'],
        ];

        $drive = driver::insert($arrayku);

    }
}
