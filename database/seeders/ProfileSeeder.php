<?php

namespace Database\Seeders;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $pass = Hash::make('password');
        $userArr = [
            ['name' => 'Admin', 'email' => 'admin@app.com', 'password' => $pass, 'level' => '1'],
            ['name' => 'Manager A', 'email' => 'managera@app.com', 'password' => $pass, 'level' => '2'],
            ['name' => 'Manager B', 'email' => 'managerb@app.com', 'password' => $pass, 'level' => '2'],
        ];
        $user = User::insert($userArr);

        $allUser = User::all();
        foreach ($allUser as $key) {
            Profile::create([
                'user_id' => $key->id,
                'first_name' => $key->name,
                'last_name' => '',
                'nip' => $key->id,
            ]);
        }
    }
}
