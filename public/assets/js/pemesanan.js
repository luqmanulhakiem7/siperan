// if ($("#pemesananOverview").length) {
//     var pemesananOverviewChart = document
//         .getElementById("pemesananOverview")
//         .getContext("2d");
//     var pemesananOverviewData = {
//         labels: [
//             "JAN",
//             "FEB",
//             "MAR",
//             "APR",
//             "MAY",
//             "JUN",
//             "JUL",
//             "AUG",
//             "SEP",
//             "OCT",
//             "NOV",
//             "DEC",
//         ],
//         datasets: [
//             {
//                 label: "Ditolak",
//                 data: [0, 0, 0, , 0, 0, 0, 10, 0, 0, 0, 0],
//                 backgroundColor: "#52CDFF",
//                 borderColor: ["#52CDFF"],
//                 borderWidth: 0,
//                 fill: true, // 3: no fill
//             },
//             {
//                 label: "Disetujui",
//                 data: [0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0],
//                 backgroundColor: "#1F3BB3",
//                 borderColor: ["#1F3BB3"],
//                 borderWidth: 0,
//                 fill: true, // 3: no fill
//             },
//         ],
//     };

//     var pemesananOverviewOptions = {
//         responsive: true,
//         maintainAspectRatio: false,
//         scales: {
//             yAxes: [
//                 {
//                     gridLines: {
//                         display: true,
//                         drawBorder: false,
//                         color: "#F0F0F0",
//                         zeroLineColor: "#F0F0F0",
//                     },
//                     ticks: {
//                         beginAtZero: true,
//                         autoSkip: true,
//                         maxTicksLimit: 5,
//                         fontSize: 10,
//                         color: "#6B778C",
//                     },
//                 },
//             ],
//             xAxes: [
//                 {
//                     stacked: true,
//                     barPercentage: 0.35,
//                     gridLines: {
//                         display: false,
//                         drawBorder: false,
//                     },
//                     ticks: {
//                         beginAtZero: false,
//                         autoSkip: true,
//                         maxTicksLimit: 12,
//                         fontSize: 10,
//                         color: "#6B778C",
//                     },
//                 },
//             ],
//         },
//         legend: false,
//         legendCallback: function (chart) {
//             var text = [];
//             text.push('<div class="chartjs-legend"><ul>');
//             for (var i = 0; i < chart.data.datasets.length; i++) {
//                 console.log(chart.data.datasets[i]); // see what's inside the obj.
//                 text.push('<li class="text-muted text-small">');
//                 text.push(
//                     '<span style="background-color:' +
//                         chart.data.datasets[i].borderColor +
//                         '">' +
//                         "</span>"
//                 );
//                 text.push(chart.data.datasets[i].label);
//                 text.push("</li>");
//             }
//             text.push("</ul></div>");
//             return text.join("");
//         },

//         elements: {
//             line: {
//                 tension: 0.4,
//             },
//         },
//         tooltips: {
//             backgroundColor: "rgba(31, 59, 179, 1)",
//         },
//     };
//     var pemesananOverview = new Chart(pemesananOverviewChart, {
//         type: "bar",
//         data: pemesananOverviewData,
//         options: pemesananOverviewOptions,
//     });
//     document.getElementById("marketing-overview-legend").innerHTML =
//         pemesananOverview.generateLegend();
// }
