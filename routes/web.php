<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\VehicleRequestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/dashboard', function () {
//     return view('pages.Dashboard.index');
// });
Route::get('/', function () {
    return view('pages.login.login');
});
Route::get('/login', function () {
    return view('pages.login.login');
});
Route::post('try-login', [AuthController::class, 'login'])->name('try-login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::get('dashboard', [HomeController::class, 'index']);
Route::get('driver', [DriverController::class, 'index']);

Route::get('angkutan', [VehicleController::class, 'indexAll']);
Route::get('angkutan-barang', [VehicleController::class, 'indexBarang']);
Route::get('angkutan-orang', [VehicleController::class, 'indexOrang']);

Route::get('pemesanan-kendaraan', [VehicleRequestController::class, 'index']);
Route::get('approve', [VehicleRequestController::class, 'approve']);
Route::get('waiting-approval', [VehicleRequestController::class, 'waiting']);
Route::get('history-approval', [VehicleRequestController::class, 'history']);
Route::get('laporan-approval', [VehicleRequestController::class, 'indexApprove']);
Route::get('approval/{idRequest}/{idApprove}', [VehicleRequestController::class, 'update']);
Route::post('store-pemesanan-kendaraan', [VehicleRequestController::class, 'store'])->name('simpan-pemesanan');

Route::get('laporan/export', [VehicleRequestController::class, 'export']);